import dash
import dash_core_components as dcc
import dash_html_components as html

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    #html.H1('Hello to my website :)', style={'textAlign': 'center', 'color': '#7FDBFF'}),
    html.Label('Dropdown'),
    dcc.Dropdown(
        options=[
            {'label': 'Cairo', 'value': 'NYC'},
            {'label': 'Alexandria', 'value': 'MTL'},
            {'label': 'Aswan', 'value': 'SF'}
        ],
        value='MTL'
    ),

    html.Label('Multi-Select Dropdown'),
    dcc.Dropdown(
        options=[
            {'label': 'Cairo', 'value': 'NYC'},
            {'label': 'Alexandria', 'value': 'MTL'},
            {'label': 'Aswan', 'value': 'SF'}
        ],
        value=['MTL', 'SF'],
        multi=True
    ),

    html.Label('Radio Items'),
    dcc.RadioItems(
        options=[
            {'label': 'Cairo', 'value': 'NYC'},
            {'label': 'Alexandria', 'value': 'MTL'},
            {'label': 'Aswan', 'value': 'SF'}
        ],
        value='MTL'
    ),

    html.Label('Checkboxes'),
    dcc.Checklist(
        options=[
            {'label': 'Cairo', 'value': 'NYC'},
            {'label': 'Alexandria', 'value': 'MTL'},
            {'label': 'Aswan', 'value': 'SF'}
        ],
        values=['MTL', 'SF']
    ),

    html.Label('Text Input'),
    dcc.Input(value='MTL', type='text'),

    html.Label('Slider'),
    dcc.Slider(
        min=0,
        max=9,
        marks={i: 'Label {}'.format(i) if i == 1 else str(i) for i in range(1, 6)},
        value=5,
    ),
dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'Cairo'},
                {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': 'Alexandria'},
            ],
            'layout': {
                'title': 'Data Visualization'
            }
        }
    )], style={'columnCount': 2})

if __name__ == '__main__':
    app.run_server(debug=False,port=8050,host='0.0.0.0')